import java.util.Date;
import java.util.ArrayList;
import com.devcamp.s52javabasic.Order;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order> orderList = new ArrayList<>();
        // khởi tạo 4 tham số khác nhau
        Order Order1 = new Order();
        Order Order2 = new Order("Lan");
        Order Order3 = new Order(3,"Long",80000);
        Order Order4 = new Order(4,"Nam",75000,new Date(),false,new String[] {"hop mau","tay","giay mau"});
        orderList.add(Order1);
        orderList.add(Order2);
        orderList.add(Order3);
        orderList.add(Order4);

        for(Order order : orderList) {
            System.out.println(order.toString());
        }
    }
}
