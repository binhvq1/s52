package com.devcamp.s52javabasic;
// import java.lang.reflect.Array;
import java.util.Locale;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

// import javax.tools.DocumentationTool.Location;

public class Order {
    int id; // id của order
    String customerName; // tên khách hàng
    long price;// tổng giá tiền
    Date orderDate; // ngày thực hiện order
    boolean confirm; // đã xác nhận hay chưa?
    String[] items; // danh sách mặt hàng đã mua

     // khởi tại không tham số
     public Order() {
        this.id = 1;
        this.customerName = "2";
        this.price = 120000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] {"book","pen","rule"};
    }

    public Order(String customerName) {
        this.id = 1;
        this.customerName = customerName;
        this.price = 120000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] {"book","pen","rule"};
    }

    // khởi tạo 3 tham số
    public Order(int id, String customerName, long price) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
    }

    // khởi tạo tất cả tham số
    public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = true;
        this.items = items;
    }

    @Override
    public String toString() {
        // định dạng tiêu chuẩn VN
        Locale.setDefault(new Locale("vi","VN"));
        String pattern = "yyyy-MM-dd";
        DateTimeFormatter defaultTimeFormat = DateTimeFormatter.ofPattern(pattern);
        String timeStamp;
        if(orderDate == null) {
            timeStamp = new SimpleDateFormat("yyyy/MM/dd").format(Calendar.getInstance().getTime());
        } else {
            timeStamp = defaultTimeFormat.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        }
        // định dạng cho giá tiền
        Locale uslocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(uslocale);
        return "Order [id=" + id + "customerName=" + customerName +", price="+ usNumberFormat.format(price)
        +", confirm: "+ confirm +", items= "+ Arrays.toString(items) + ", orderDate=" + timeStamp + "]";
    }
}


