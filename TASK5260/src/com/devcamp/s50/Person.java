package com.devcamp.s50;

public class Person {
    String name;
    int age;

    public Person(String Name,int age) {
        this.name = Name;
        this.age = age;
    }
    public Person(String Name) {
        this.name = Name;
    }
    

    public void printName() {
        System.out.println(this.name);
    }
}
