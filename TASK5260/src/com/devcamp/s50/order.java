package com.devcamp.s50;
import java.util.Date;
// Import  thư viện
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class order {
    int id; // id của order
    String customerName; // tên khách hàng
    long price;// tổng giá tiền
    Date orderDate; // ngày thực hiện order
    boolean confirm; // đã xác nhận hay chưa?
    String[] items; // danh sách mặt hàng đã mua
    // Khởi tạo 1 tham số name

    public order(String name) {
        this.id = 1;
        this.customerName = name;
        this.price = 20000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] { "book", "pen", "rule" };
    }
    public order(int id,String customerName,long price,Date orderDate,boolean confirm,String[] items){
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = true;
        this.items = new String[] { "book", "pen", "rule" };
    }
    public order(){
        this("2");
    }
    public order(int id,String customerName,long price){
        this(id,customerName,price,new Date(),true,new String[] { "toan", "van", "hung" });
    }
    @Override
    public String toString(){
        Locale.setDefault(new Locale("us","us"));
        // định dạng cho ngày tháng
        String pattern = "dd-MM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern(pattern);
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        return
        "order [id="+id
        +",customerName="+ customerName
        +",price="+ usNumberFormat.format(price) 
        +",orderDate="+ myFormatObj.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
        +",confirm="+ confirm
        +",items="+ Arrays.toString(items);

    }
}
