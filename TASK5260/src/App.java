import java.util.ArrayList;
import java.util.Date;

import com.devcamp.s50.Person;
import com.devcamp.s50.order;
import com.devcamp.s50.order2;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<order2> mArrayList = new ArrayList<order2>();
        Person person1 = new Person("Nguyen Van Teo",20);
        Person person2 = new Person("Nguyen Van Tuan");
        order2 order1 = new order2("Binh", person2);
        // order order2 = new order("lan");
        // order order3 = new order(3,"long",22000);
        order2 order4 = new order2(4, "Binh", 30000, new Date(), false, new String[] { "teo", "leo", "peo" }, person1);
        mArrayList.add(order1);
        // mArrayList.add(order2);
        // mArrayList.add(order3);
        mArrayList.add(order4);
        // in ra màn hình
        for (order2 order : mArrayList) {
            System.out.println(order.toString());
        }
    }

}
